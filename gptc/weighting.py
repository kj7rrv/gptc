# SPDX-License-Identifier: GPL-3.0-or-later

import math
from typing import Sequence, Tuple, List


def _mean(numbers: Sequence[float]) -> float:
    """Calculate the mean of a group of numbers

    Parameters
    ----------
    numbers : list of int or float
        The numbers to calculate the mean of

    Returns
    -------
    float
        The mean of the numbers
    """
    return sum(numbers) / len(numbers)


def _standard_deviation(numbers: Sequence[float]) -> float:
    """Calculate the standard deviation of a group of numbers

    Parameters
    ----------
    numbers : list of int or float
        The numbers to calculate the mean of

    Returns
    -------
    float
        The standard deviation of the numbers

    """
    mean = _mean(numbers)
    squared_deviations = [(mean - i) ** 2 for i in numbers]
    return math.sqrt(_mean(squared_deviations))


def weight(numbers: Sequence[float]) -> Tuple[float, List[float]]:
    standard_deviation = _standard_deviation(numbers)
    weight_assigned = standard_deviation * 2
    weighted_numbers = [i * weight_assigned for i in numbers]
    return weight_assigned, weighted_numbers
