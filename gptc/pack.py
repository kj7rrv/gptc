# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import os
from typing import List, Dict, Tuple


def pack(
    directory: str, print_exceptions: bool = False
) -> Tuple[List[Dict[str, str]], List[Tuple[OSError]]]:
    paths = os.listdir(directory)
    texts: Dict[str, List[str]] = {}
    exceptions = []

    for path in paths:
        texts[path] = []
        try:
            for file in os.listdir(os.path.join(directory, path)):
                try:
                    with open(
                        os.path.join(directory, path, file), encoding="utf-8"
                    ) as input_file:
                        texts[path].append(input_file.read())
                except OSError as error:
                    exceptions.append((error,))
                    if print_exceptions:
                        print(error, file=sys.stderr)
        except OSError as error:
            exceptions.append((error,))
            if print_exceptions:
                print(error, file=sys.stderr)

    raw_model = []

    for category, cat_texts in texts.items():
        raw_model += [{"category": category, "text": i} for i in cat_texts]

    return raw_model, exceptions
