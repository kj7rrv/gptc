# SPDX-License-Identifier: GPL-3.0-or-later

import cProfile
import gptc
import json
import sys

max_ngram_length = 10

with open("models/raw.json") as f:
    raw_model = json.load(f)

with open("models/benchmark_text.txt") as f:
    text = f.read()

cProfile.run("gptc.Model.compile(raw_model, max_ngram_length)")
